## Competenza
Competenza esclusiva dello Stato
Regioni interventi per rimuovere ostacoli economici e sociali
## Livelli essenziali prestazioni
Servizi minimi diritto studente
Importo borsa studio per
- materiale didattico
- trasporto
- ristorazione
- alloggio
- spese per cultura e formazione
## Borse di studio ed esoneri
In base a ISEE, indicatore situazione economica all'estero o indicatore della situazione patrimoniale equivalente
Esonero tassa d'iscrizione e contributi
- studenti con invalidità >66%
- beneficiari borsa di studio annuale Governo italiano
- studenti intenzionati a ricongiungere carriera dopo 2 anni di interruzione
