Testo Unico del Pubblico Impiego
## Rapporto di pubblico impiego
- volontario
- personale
- bilaterale
- subordinato
disciplina privatistica
## Testo unico
- aumento efficienza amministrazioni
- razionalizzazione costo lavoro pubblico
- miglioramento utilizzazione risorse umane
omogeneizzazione parziale diritto lavoro pubblico e privato
## Riforma Brunetta
Distinzione funzioni di indirizzo e controllo Governo e funzione gestione dirigenza
Indicatore di performance dirigente collegati all'ambito
Mancato raggiungimento obiettivi comporta sanzioni quali revoca incarico o licenziamento
## Riforma Madia
14 deleghe legislative impugnate per la maggior parte da ricorso Regione Veneto
## Accesso al pubblico impiego
- concorsi interni
- concorsi misti
- concorsi interni misti
## Lavoratore pubblico
## Contrattazione collettiva del personale non dirigente
Dopo battuta arresto 2008-2018 rinnovo tutti i contratti nazionali pubblici dipendenti non dirigenziali
Contratto collettivo nazionale quadro del 2016
### Classificazione personale
- categoria B
- categoria C
- categoria D
- categoria EP
## Trattamento economico
principio di onnicomprensività della retribuzione
- trattamento fondamentale
- trattamento accessorio
## Diritti non patrimoniali e doveri dipendente
- diritto all'ufficio
- diritto allo svolgimento delle mansioni
- diritto alla progressione
- diritto al riposo
- diritto alla riservatezza
- diritto alle pari opportunità
- diritto di accesso agli atti e ai documenti
- diritti sindacali
## Responsabilità dipendente
giudizio di responsabilità
tipologie di danno erariale
- di natura patrimoniale
- da disservizio
- all'immagine della Pubblica Amministrazione
