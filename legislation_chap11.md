- professore ordinario
- professore associato
- professore aggregato
- professore a contratto
- ricercatore universitario
## Meccanismo di reclutamento
- fase nazionale
- fase a livello universitaria
### Abilitazione scientifica nazionale
durata 9 anni
domanda al Ministero corredata di titoli e pubblicazioni inviata per via telematica
criteri valutazione verificati ogni 5 anni
Commissione nazionale 5 membri mandato 2 anni
attribuzione abilitazione a maggioranza entro 4 mesi da termine quadrimestre candidatura
### Chiamata dei professori
- procedimento pubblico sulla Gazzetta ufficiale, sito ateneo e Ministero
- ammissione studiosi in possesso di abilitazione
- valutazione CV, attività didattica e pubblicazioni
- proposta ad opera Dipartimento con maggioranza dei professori di stessa fascia e superiore
## Ricercatori
Dal 2020 contratti non più tempo indeterminato
Carta europea dei ricercatori
Contratti ricercatori
- triennali, 1 proroga 2 anni
- triennali non rinnovabili
contratti a tempo pieno (350 ore) o tempo definito (200 ore)
ricercatore -> professore associato oppure rapporto lavoro termina
## Dottorato di ricerca
3-5 anni
Percorsi
- ricerca, indagine e approfondimento ambito o settore specifico
- progetto di ricerca presentato nella domanda
Relazione per ammissione anno successivo
Tesi di dottorato alla conclusione
