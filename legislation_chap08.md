provvedimento amministrativo o contratto
Contratti attivi e passivi
- contratti ordinari
- contratti speciali di diritto privato
- contratti ad oggetto pubblico
## Codice
- Cabina di regia
- ANAC
- Ministero delle infrastrutture e delle mobilità sostenibili
- Consiglio superiore dei lavori pubblici
Soggetti
- amministrazioni aggiudicatrici
- enti aggiudicatori
- concessionario
- promotore
- offerente
- candidato
Contratti esclusi
- affidamenti in-house
- contratti di sponsorizzazione
## Responsabile unico (RUP)
## Fase di programmazione
- Programma biennale degli acquisti di beni e servizi
  Importo unitario >= 40000€
- Programma triennale dei lavori pubblici
  Importo >= 100000€
## Fase di progettazione
- Progettazione dei lavori pubblici
  - progetto di fattibilità tecnica ed economica
  - progetto definitivo
  - progetto esecutivo
- Progettazione di servizi e forniture
## Fase procedurale
- deliberazione a contrarre
- indizione gara
- procedura di evidenza pubblica
### Procedure
- procedura aperta
- procedura ristretta
- paternariato per l'innovazione
- procedura competitiva con negoziazione
- dialogo competitivo
- procedure negoziate senza bando di gara
### Selezione contraente
Avviso di pre-informazione entro 31 dicembre pubblicato su
- profilo committente
- piattaforma digitale ANAC
Principio di tassatività delle cause di esclusione
Soccorso istruttorio in caso di carenze
### Disciplina dei contratti sotto soglia
- affidamento diretto < 40000€
- affidamento diretto con valutazione 3 (lavori) o 5 (servizi e forniture) preventivi 40000-150000€
- procedura negoziata con consultazione >15 operatori economici 150000-350000€
- procedura negoziata con consultazione >15 operatori economici senza bando di gara 350000-1000000€
- procedura aperte >1000000€
## Aggiudicazione
### Criteri
- offerta economicamente più vantaggiosa
  - servizi sociali e di ristorazione ospedaliera, assistenziale e scolastica, servizi ad alta intensità di manodopera
  - servizi di ingegneria e architettura e altri di natura tecnica-intellettuale importo <40000€
  - servizi e forniture alto contenuto tecnologico o carattere innovativo importo >40000€
- criterio del minor prezzo
  - servizi e forniture con caratteristiche standardizzate o definite dal mercato
  - servizi o forniture di importo <40000€
  - lavori di importo <=1000000€
Rating di legalità
