Fondato MURST -> MIUR
MIUR -> MPI e MUR (riaccorpati nel 2008)
Compiti MUR:
- indirizzo, programmazione e coordinamento della ricerca
- attuazione delle normee degli accordi europei e internazionali
- gestione rapporto con ANVUR
- vigilanza INVALSI e INDIRE
- promozione ricerca nelle imprese
## Organizzazione interna
- Direzione generale delle istituzioni della formazione superiore
- Direzione  generale degli ordinamenti della formazione superiore e del diritto allo studio
- Direzione generale della ricerca
- Direzione generale dell'internalizzazione e della comunicazione
- Direzione generale del personale, del bilancio e dei servizi strumentali
- Segretariato generale
## Organi consultivi
- ANVUR
  - Presidente
  - Consiglio direttivo
  - Collegio dei Revisori dei Conti
- CUN
  - Presidente
  - max 14 tra professori e ricercatori
  - 8 studenti di diversi dipartimenti
  - 3 membri personale tecnico-amministrativo
  - 3 membri CRUI (Rettori)
  - 1 membro Coordinamento Nazionale delle Conferenze dei Presidi di Facoltà
  - 1 membro Convegno permanente dei Dirigenti Amministrativi delle Università
- CNSU
  - 28 iscritti a corsi di laurea, laurea magistrale e scuole dirette a fini speciali
  - 1 iscritto a corsi di specializzazione
  - 1 iscritto a corsi di dottorato e ricerca
  In carica 3 anni con 1 rielezione
- CRUI
  - Presidente
  - 2 Vicepresidenti
  - Assemblea generale
  - Giunta
  - Segretario generale
  - Direttore
  - Collegio dei Revisori
- CODAU
  - Presidente
  - Assemblea generale
  - Giunta
  - Collegio dei Revisori dei Conti
- Commissione di esperti
  - 3 membri CNPI
  - 3 membri CUN
  - 2 membri CNEL
  - 1 rappresentante IRRSAE
  - 6 esperti designati dal MinistroA
- CNAM
- CEPR
  - Ministro
  - 9 membri altamente qualificati
## Fondi
- Fondo per il finanziamento ordinario delle università
  - Fondo per il finanziamento delle attività di base di ricerca
- Fondo per l'edilizia universitaria e per le grandi attrezzature scientifiche
- Fondo per la programmazione dello sviluppo del sistema universitario
- Fondo per gli investimenti nella ricerca scientifica e tecnologica
- Fondo per il merito
- Fondo per la formazione e l'aggiornamento della dirigenza
- Fondo per i poli universitari tecnico-scientifici nel Mezzogiorno
