## Trasferimenti dello stato
- Fondo per il finanziamento ordinario delle università
  - quota base
  - quota riequilibrio
  in 2021 8 milioni di € 30% quota premiale in base a VQR e Valutazione delle politiche di reclutamento
  Costo unitario per studente 32% FFO 2021-2023
  - personale
  - gestione
  - funzionamento
  Dal 2011 1.5% per sottofinanziamento > 5% previsioni ANVUR
- Fondo per l'edilizia universitaria e per le grandi attrezzatture
  In 2021 circa 1.400.000,00
- Fondo per la programmazione dello sviluppo del sistema universitario (abolito nel 2014)
## Contributi obbligatori
Contributi annuali versati da studenti in base a disponibilità economica
## Indebitamento
Mutui e altre forme in base ad indicatore
Indicatore = (capitale + interessi) / (contributi statali + tasse + soprattasse + contributi universitari)
Indicatore <= 15% permette indebitamento
