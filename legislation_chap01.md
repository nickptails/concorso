Centro istituzionale di formazione culturale e di attività di ricerca
Libertà culturale garantita tramite:
- accesso allo studio
- libertà di insegnamento
Principio pluralismo della Pubblica Amministrazione:
- Enti autarchici
  - territoriali o non territoriali
  - strumentali o ausiliari
- Enti economici
Università ente autarchico in senso stretto
## Processo di riforma
Istituzione MUR
Università dotate di personalità giuridica
Autonomia didattica, scientifica, organizzativa, finanziaria e contabile
Riforma Gelmini
- organizzazione e governance del sistema universitario
- reclutamento di giovani studiosi
- accesso di giovani studiosi
- erogazione risorse
- introduzione contabilità economico-patrimoniale uniforme
## Autonomia
Autonomia, indipendenza e autogoverno
### Normativa
Possibilità di emanare propri statuti e regolamenti
Organo preposto con decreto rettorale formato da 15 membri:
- Rettore
- Senato Accademico
- Rappresentanti studenti
- Consiglio di Amministrazione
Deliberati dal Senato Accademico con assenso del Consiglio di Amministrazione
Trasmessi al Ministro, 120 giorni per controllo
Dopo un riesame riapprovato con:
- maggioranza dei 3/5 per difetti di legittimità
- maggioranza assoluta per difetti di merito
Statuto:
- organi
- funzioni dipartimento
- organizzazione dipartimento
- istituzione commissione paritetica docenti-studenti
- sanzioni per codice etico
- corsi di laurea e specializzazione
- criteri per dottorato di ricerca
- strutture didattiche
- cariche accademiche
Regolamenti da Senato Accademico o Consiglio di Amministrazione nel Bollettino ufficiale Ministero
Controllo ministeriale necessario per:
- regolamento didattico di ateneo (validato da decreto rettorale)
  - criteri di accesso ai corsi di laurea
  - obiettivi, tempi e modalità
  - procedure attribuzione dei compiti didattici annuali a professori e ricercatori
  - procedure svolgimento esami
  - modalità valutazione
  - servizio di orientamento
  - forme di pubblicità procedure e decisioni
  - metodi rilascio titoli
- regolamento generale di ateneo
- regolamento per l'amministrazione, la finanza e la contabilità
### Finanziaria
Individuazione delle fonti economiche e gestione dei fondi
Spetta al Consiglio di Amministrazione:
- approvare il regolamento
- stabilire l'indirizzo strategico
- approvare il programma finanziario e del personale
- vigilare sulla programmazione
- approvare il bilancio di previsione annuale e triennale e il conto consuntivo (-> MUR)
### Didattica
Libertà di disciplinare l'ordinamento degli studi e dei corsi
- Piano didattico annuale (60 crediti)
- percorso professionalizzante di 2 anni
- biennio metodologico-formativo (120 crediti)
Regolamento corso di studio:
- elenco degli insegnamenti
- obiettivi formativi
- curricula offerti
- tipologie didattiche
### Organizzativa
Apparato amministrativo con almeno:
- ufficio rettorato
- ufficio personale e risorse umane
- segreteria direzione generale
- ufficio segreteria organi collegiali
### Codice etico
Collegio di disciplina vigila sui principi e valore del Codice etico
In caso di infrazione:
- Rettore apre procedimento trasmesso al Collegio
- Collegio ascolta le parti e entro 30 giorni emana sanzione
- Consiglio di Amministrazione conferma sanzione o archivia il procedimento
