## Soggetti
CIVIT e in parte Dipartimento Funzione Pubblica -> ANAC
### ANAC
AVCP -> ANAC
Segnalazioni cittadini, dirigenti pubblici e avvocati dello Stato
Autorità indipendente con poteri sanzionatori amministrativi
- potere di regolazione
- compiti di vigilanza
- poteri di intervento
- poteri ispettivi
- controlla operato Responsabili della Trasparenza
- chiede a OIV informazioni sull'adempimento degli obblighi di trasparenza
Enti vigilati
- Enti pubblici
- Società partecipate
- enti privati sotto il controllo di amministrazioni pubbliche
### Organo di indirizzo politico e responsabile prevenzione
- Organo di indirizzo politico
  individua tra dirigenti amministrativi di ruolo di I fascia il RPC
  Adotta entro il 31 gennaio su proposta del RPC Piano triennale prevenzione della corruzione e lo trasmette al Dipartimento Funzione Pubblica
- Responsabile per la Prevenzione della Corruzione
  Propone Piano triennale entro 31 gennaio
  Definisce entro 31 dicembre procedure per dipendenti in settori a rischio
  Verifica attuazione piano e idoneità e propone modifiche
  Verifica d'accordo con il dirigente preposto rotazione incarichi in settore a rischio
  Individua personale da formare
  Pubblica relazione operato svolto entro 15 dicembre
### Ruoli di supporto
- Prefetto
- Scuola superiore della pubblica amministrazione
## Piano nazionale anticorruzione
PNA prevede che ogni amministrazione PTCPT
PNA redatto ogni 3 anni aggiornato annualmente entro 31 gennaio
## Piano integrato di attività e organizzazione
PIAO durata 3 anni aggiornato annualmente
## Sistema anticorruzione
- funzione di indirizzo spartita tra Rettore, Senato, Consiglio di amministrazione
- funzione di gestione al Direttore generale
- funzione di controllo finanziario al Collegio dei Revisori dei conti
- funzioni di verifica, controllo e valutazione al Nucleo di Valutazione con OIVA
Piano anticorruzione aggiornato dal RPC approvato dal Consiglio di amministrazione previa informativa Senato accademico
RPCT selezionato tra dirigenti I fascia o si identifica con direttore generale
RPCT collabora con RAT e Nucleo di Valutazione
### Amministrazione trasparente
Pubblicazione informazioni su organizzazione e procedimenti amministrativi nei siti web istituzionali
