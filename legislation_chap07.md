Regolamento edi ateneo emanato dal Rettore su deliberazione Consiglio di amministrazione su parere Senato
Sistema di contabilità economico-patrimoniale
## Contabilità economica-patrimoniale
- aspetto finanziario (debiti e crediti)
- aspetto economico (costi e ricavi)
sistema di contabilità finanziaria -> sistema contabile economico-patrimoniale
- trasparenza e omogeneità
- individuazione situazione patrimoniale
- valutamento gestione
sistema di contabilità analitica
- centri di responsabilità
- centri di costo
- costi dei fattori produttivi
controllo di gestione
- predisposizione del piano
- rilevazione costi e ricavi
- valutazione dei dati
## Principi contabili
## Tracciabilità dei flussi finanziari
Dal 2006 SIOPE
Dal 2019 SIOPE+ pagamenti attraverso OPI secondo standard di AGID
## Documenti bilancio
- bilancio unico d'ateneo di previsione annuale
  approvato dal Consiglio di amministrazione su proposta del Rettore previo parere del Senato
  budget economico e budget degli investimenti
  predisposto entro 31 dicembre
- bilancio unico d'ateneo di previsione triennale
  budget economico e budget degli investimenti
  predisposto entro 31 dicembre
- bilancio unico d'ateneo di esercizio
  stato patrimoniale, conto economico, rendiconto finanziario e nota integrativa + relazione Collegio dei Revisori dei conti
- bilancio consolidato
  stato patrimoniale, conto economico e nota integrativa
## Forme di controllo finanziario
sistema tesoreria unica anni '80 -> sistema misto anni '90 applicato 2009-2012 posticipato al 2025
- controllo esterno dalla Corte dei conti, in fase sia preventiva sia successiva
- controllo interno dai Nuclei di Valutazione
Dichiarazione di dissesto finanziario
- entro il 30 aprile relazione Collegio dei Revisori dei conti
- su relazione negativa Consiglio di amministrazione dichiara dissesto
- entro 6 mesi Rettore approva piano di rientro
- piano approvato dal Consiglio con assenso Senato e inviato entro 10 giorni al MUR e al MEF
- approvato dal MUR e dal MEF entro 30 giorni
Commissariamento ateneo dopo 6 mesi senza piano di rientro da Consiglio dei Ministro
- esaminano documenti entro 120 giorni
- individua creditori in 60 giorni
- redige relazione su esecuzione piano di rientro entro 30 aprile
- entro 30 giorni relazione al MUR, al MEF e alla Procura regionale Corte dei Conti
