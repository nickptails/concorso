Gazzetta Ufficiale della Repubblica Italiana -> Inpa Corncorsi Pubblici
## Bando di concorso
Dal 1° novembre su portale Inpa Concorsi Pubblici per amministrazioni e candidati
Bando di concorso:
- numero posti disponibili, riservati a personale interno e per categorie protette
- requisiti di ammissione
- cittadinanza italiana
- posizione regolare per servizio militare
- godimento diritti politici e civili
- idoneità fisica all'impiego
Ente banditore deve rispettare il bando, pena annullamento procedura
## Domanda
Documenti:
- carta d'identità
- CV
- certificazione per portatori di handicap per assistenza nel corso della prova
- ricevuta versamento tassa
Informazioni telematizzate che richiedono SPID
Erronea presentazione -> esclusione
## Scelta
numero di posizioni aperte corrisponde a numero aspiranti
procedura porta a graduatoria e idoneità utile per contratti futuri
## Prova preselettiva
Soglia di candidature superata
Quiz a risposta multipla
Materie disciplinate nel bando + domande di logica e cultura generale
Domande di logica in banca dati 1000-5000 quesiti
Domande di cultura generale su storia contemporanea, geografia e attualità
## Prova scritta
Tipologie
- domande a risposte aperta
- tema
## Prova orale
Quesiti orali presentati da commissione
Padronanza linguistica e dell'argomento trattato con collegamenti
## Riforma concorsi pubblici
- Una sola prova scritta e una orale
- Considerazione titoli conseguiti
- Utilizzo strumenti informatici
