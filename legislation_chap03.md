Sostegno economico dello Stato
Forme di autofinanziamento 10-15% entrate
Università non statali sostegno 30-40%
20% contributi spartito tra università
- 77 università statali
- 19 università non statali
- 11 università telematiche
## Autonomia didattica
- regolamento didattico di ateneo
- Decreti ministeriali
## Didattica
  - laurea
  - laurea magistrale
  - diploma di specializzazione
  - dottorato di ricerca
  - master
  CFU = 25 ore di lavoro
  60 CFU annuali
  corsi di studio attivati con assenso Nucleo Valutazione
  - autovalutazione
  - valutazione
  - accreditamento
  ### Studi
  corso di laurea 180 CFU fornisce accesso
  - attività lavorativa
  - corso di laurea magistrale
  - master di I livello
  - corsi di alta formazione e aggiornamento
  corso di laurea magistrale 120 CFU
  corso di laurea a ciclo unico 5 o 6 anni 300-360 CFU
  corso di master universitario di I livello durata min 1 anno
  dottorato di ricerca durata 3-4 anni conclusa da tesi di ricerca
  diploma di specializzazione CFU stabiliti dal Ministero
  master di II livello
## Scuole dirette a fini speciali
diplomi post-secondari per uffici e professioni richiedenti formazione universitaria
## Scuole di specializzazione
Scuole di specializzazione area sanitaria
- concorso pubblico
- percorso triennale di formazione (FIT)
- accesso a ruoli a tempo indeterminato
Scuole di specializzazione professioni legali
corsi di perfezionamento durata 1 anno rilascia attestato di frequenza
## ISEF
diploma ISEF = diploma di laurea in scienze motorie
