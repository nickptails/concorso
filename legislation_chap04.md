Supporto soggetti pubblici e incentivazione privati
Università, enti di ricerca pubblici e privati
Assegno di ricerca: contratti 3 anni
## Funzioni del MUR
PNR dicembre 2020 attuazione 2021-2027
CNVR valuta progetti ogni 3 anni
Presidente eletto a maggioranza 2/3 carica 5 anni
## Attività di ricerca
Valutazione Qualità della Ricerca ogni 5 anni da ANVUR
Selezione Dipartimenti di eccellenza (per i 5 anni precedenti)
- alla fine del 4° anno MUR elegge commissione 7 membri
- MUR individua 350 dipartimenti
- dipartimenti presentano domanda per progetto 5 anni
- commissione sceglie 180 dipartimenti
Centri di eccellenza presso università ogni 3 anni
## Enti
### CNR
- Presidente
- Videpresidente
- Direttore generale
- Consiglio d'amministrazione
- Consiglio scientifico
- Collegio dei Revisori dei Conti
### INFN
- Presidente
- Consiglio direttivo
- Giunta esecutiva
- Collegio dei Revisori dei Conti
### ASI
- Presidente
- Consiglio d'amministrazione
- Consiglio tecnico-scientifico
- Collegio dei Revisori dei Conti
### INAF
- Presidente
- Consiglio d'amministrazione
- Consiglio scientifico
- Collegio dei Revisori dei Conti
### INGV
- Presidente
- Consiglio d'amministrazione
- Consiglio scientifico
- Collegio dei Revisori dei Conti
### CFREF
- Presidente
- Consiglio d'amministrazione
- Consiglio scientifico
- Collegio dei Revisori dei Conti
### INRIM
- Presidente
- Consiglio d'amministrazione
- Consiglio scientifico
- Direttore scientifico
- Direzione scientifica
- Collegio dei Revisori dei Conti
### Altri
- INDAM
- OGS
- IISG
- Consorzio per l'Area di Ricerca Scientifica e Tecnologica di Trieste
- Stazione Zoologica Anton Dohrn
## Cluster
Insiemi di soggetti pubblici e privati per collegare ricerca e imprese
Settimana della Cultura Scientifica
## Finanziamenti nazionali ed europei
- Fondo per la promozione e lo sviluppo delle politiche per il PNR
- Fondo per la ricerca in campo economico e sociale
- Fondo per l'edilizia e le infrastrutture di ricerca
- Fondo per la valutazione e valorizzazione progetti di ricerca
PNRR ha pacchetto per accedere a fondo europeo NGEU 6 miliardi
