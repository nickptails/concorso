# Argomenti concorso
- [Architetture HW e sistemi operativi Windows e Linux](archos.md)
- [Tecnologie di networking e protocolli di reti (stack ISO/OSI)](network.md)
- [Tecnologie e linguaggi per siti web, gestione DB](webdb.md)
- [Programmazione procedurale e ad oggetti](programming.md)
- [Tecnologie di virtualizzazione](virtualization.md)
- [Apparati e configurazione per sicurezza informatica](security.md)
- [Legislazione Universitaria](legislation.md)
- [Statuto dell'Università La Sapienza](statute.md)
- Lingua inglese
