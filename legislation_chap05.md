## Rettore
Eletto tra i professori ordinari da
- professori di ruolo
- ricercatori
- componenti Consiglio degli studenti e altri rappresentanti degli studenti
- personale tecnico-amministrativo
Prime 3 votazioni a maggioranza assoluta, poi ballottaggio tra 2 candidati più votati
Carica 6 anni non rinnovabile
Può decadere o essere sostituito da Prorettore
## Senato accademico
Max 35 unità tra cui
- Rettore
- rappresentanza di studenti
- docenti di ruolo
- direttori di dipartimento
Carica 4 anni rinnovabile 1 volta
## Consiglio di amministrazione
Max 11 componenti tra cui
- Rettore
- rappresentanza studenti
- altri scelti mediante avvisi pubblici
Carica 4 anni rinnovabile 1 volta
## Collegio dei revisori dei conti
- Presidente tra magistrati contabili, amministrativi e avvocati dello Stato
- membro effettivo e supplente da MEF
- membro effettivo e supplente da MUR
Carica 4 anni rinnovabile 1 volta
## Nucleo di valutazione
5-9 soggetti di elevata qualifica professionale
## Direttore generale
Dirigente altamente qualificato
Carica 4 anni rinnovabile assegnata da Consiglio di amministrazione su proposta Rettore con assenso Senato
## Organi consultivi
- Consiglio degli studenti
  - rappresentanti studenti dagli altri organi
  - 1 rappresentante per ogni dipartimento
  - 1 rappresentante per ogni commissione paritetica
  - rappresentanti dal CUS
- Garante di ateneo
- CUG (rinnovabile 1 volta)
## Dipartimento
Min. 35-40 tra professori e ricercatori
Per ogni dipartimento commissione paritetica per valutare offerta formativa
Organi dipartimento
- Direttore di dipartimento
  Tra professori ordinari di I fascia, professori di ruolo e ricercatori
  A maggioranza assoluta alla 1° votazione e a maggioranza relativa successivamente
  Carica 3 anni rinnovabile 1 volta
- Giunta di dipartimento
  - Direttore di dipartimento
  - 3 professori ordinari
  - 2 ricercatori
- Consiglio di dipartimento
  - professori ordinari
  - assistenti
  - ricercatori
